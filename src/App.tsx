import React, { useState, createContext } from 'react';
import { hot } from 'react-hot-loader/root';
import Navbar from './components/Navbar';
import About from './components/About';
import Projects from './components/Projects';
import Skills from './components/Skills';
import Contact from './components/Contact';
import Canvas from './components/Canvas';
import Footer from './components/Footer';
import LogosProvider from './components/Canvas/LogoLoaderProvider';
import useDarkMode from './hooks/useDarkMode';

import 'normalize.css';
import './App.sass';

export const DarkModeContext = createContext<boolean>(false);

const App: React.FC = () => {
  const [isDarkMode, toggleDarkMode] = useDarkMode();

  return (
    <main className="App">
      <Navbar
        isDarkMode={isDarkMode}
        toggleDarkMode={toggleDarkMode}
      />
      <About />
      <Skills />
      <DarkModeContext.Provider value={isDarkMode}>
        <Projects />
      </DarkModeContext.Provider>
      <Contact />
      <Footer />
    </main>
  );
};

export default hot(App);
