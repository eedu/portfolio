import React from 'react';
import Section from '../Section';
import ContentCard from '../ContentCard';

import skillsData from "../../data/skills";

import './Skills.sass';

const Skills: React.FC = () => (
  <Section id="skills" title="Skills">
    <ContentCard>
      <div className="skills__list">
        {skillsData.map((Skill, i) => {
          const Logo = Skill.logo;

          // eslint-disable-next-line react/no-array-index-key
          return (
          <div key={i} className="skill">
            <div className="skill__card">
              <Logo />
            </div>
            <div className="skill__name">
                {Skill.name}
            </div>
          </div>
          );
        })}
      </div>
    </ContentCard>
  </Section>
);

export default Skills;
