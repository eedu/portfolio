import React from 'react';
import Section from '../Section';
import Link from '../../common/link';

import { ReactComponent as EmailIcon } from '../../assets/icons/email.svg';

import MyInfo from "../../data/me";

import ContentCard from '../ContentCard';
import './Contact.sass';

const linkRenderer = (link: any, i: number) => (
  <Link title={link.name} classes="logo round contact__details-links-link" href={link.href} key={i} logo={link.logo}></Link>
);

const Contact: React.FC = () => (
  <Section id="contact">
    <ContentCard>
      <div className="contact__container">
        <div className="contact__details">
          <h3 className="contact__details-title">Contact Information</h3>
          <div className="contact__details-phone_number">
            <span>{MyInfo.phone_number}</span>
          </div>
          <div className="contact__details-email">
            <a className="email__link" href={`mailto:MyInfo.email`}>
                {MyInfo.email}
            </a>
          </div>
          <div className="contact__details-links">
            {MyInfo.links.map(linkRenderer)}
          </div>
        </div>
        
      </div>
    </ContentCard>
  </Section>
);

export default Contact;
