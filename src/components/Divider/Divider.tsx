import React from 'react';

import './Divider.sass';

const Divider: React.FC = () => <div className="divider" />;

export default Divider;
