import React from 'react';
import Section from '../Section';
import ContentCard from '../ContentCard';
import Emoji from '../Emoji';
import Link from '../../common/link';

import MyInfo from "../../data/me";

import './About.sass';

const renderParagraph = (text: string, i: number) => (
  <p key={i} className="about__info">{text}</p>
);

const linkRenderer = (link: any, i: number) => (
  <Link classes="my-links__link logo round" title={link.name} href={link.href} key={i} logo={link.logo}></Link>
);

const About: React.FC = () => (
  <Section id="about" classes="about__section">
    <ContentCard>
      <header className="about">
        <div className="about__title-container">
          <div className="photo">
            <img className="me" src={MyInfo.picture} alt={MyInfo.name} />
          </div>
          <h1 className="about__title">
            Hi
            {', '}
            {' '}
            I&apos;m {MyInfo.name}.
          </h1>
        </div>
        {MyInfo.description.map(renderParagraph)}
      </header>
    </ContentCard>
  </Section>
);

export default About;
