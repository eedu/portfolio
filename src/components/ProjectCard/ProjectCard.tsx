import React, { useContext } from 'react';
import Divider from '../Divider';
import Link from '../../common/link';
import { DarkModeContext } from '../../App';
import { ProjectCardProps } from '../../data/projects';
import { ReactComponent as GithubLogo } from '../../assets/logos/github.svg';

import './ProjectCard.sass';

const renderTechStack = (title: string, stack: string[]) => (
  <div className="tech-stack__category">
    <h3 className="tech-stack__title">{title}</h3>
    <ul className="tech-stack__list">
      {stack.map(tech => (
        <li key={tech} className="tech-stack__list-item">{tech}</li>
      ))}
    </ul>
  </div>
);

const ProjectCard: React.FC<ProjectCardProps> = ({
  name,
  description,
  screenshot,
  darkScreenshot,
  backend,
  frontend,
  db,
  href,
  github,
}) => {
  const isDarkMode = useContext(DarkModeContext);

  return (
    <div className="project__card">
      <h1 className="project__card-name">{name}</h1>
      <h3 className="project__card-description">{description}</h3>
      <div className="project__img-container">
        <img
          className="project__img"
          src={isDarkMode && darkScreenshot ? darkScreenshot : screenshot}
          alt={`${name} homepage`}
        />
      </div>
      <div className="tech-stack">
        <h2 className="tech-stack__header">Tech Stack</h2>
        <Divider />
        <div className="tech-stack__category-container">
          {frontend && renderTechStack('Front End', frontend)}
          {backend && renderTechStack('Back End', backend)}
          {db && renderTechStack('Database', db)}
        </div>
      </div>
      <div style={{ marginTop: 'auto' }}><Divider /></div>
      <div className="project__card-footer">
        <div className="fill" />
        {href && <Link href={href} classes="project__card-link">
          Open Project
        </Link>}
        {github
          ? <Link href={github} classes="project__card-github-link logo round"><GithubLogo className="github-logo" /></Link>
          : <div className="fill" />
        }
      </div>
    </div>
  );
};

export default ProjectCard;
