import React from 'react';

import './Link.sass';

interface LinkProps {
  title?: string;
  href: string;
  key?: any;
  logo?: any;
  classes?: string;
  children?: any;
}

const Link: React.FC<LinkProps> = ({ title, classes, logo, href, children, key = '' }) => {
  const Logo = logo;
  return (<a
    key={key}
    title={title}
    className={classes + " link"}
    href={href}
    target="_blank"
    rel="noopener noreferrer"
  >
      {logo && <Logo />}
      {children}
  </a>);
};

export default Link;
