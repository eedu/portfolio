import { ReactComponent as HTML5Logo } from '../assets/logos/html5.svg';
import { ReactComponent as CSS3Logo } from '../assets/logos/css3.svg';
import { ReactComponent as JavaScriptLogo } from '../assets/logos/javascript.svg';
import { ReactComponent as ReduxLogo } from '../assets/logos/redux.svg';
import { ReactComponent as TypeScriptLogo } from '../assets/logos/typescript.svg';
import { ReactComponent as ReactLogo } from '../assets/logos/react.svg';
import { ReactComponent as SASSLogo } from '../assets/logos/sass.svg';
import { ReactComponent as GraphQLLogo } from '../assets/logos/graphql.svg';
import { ReactComponent as NodeJSLogo } from '../assets/logos/nodejs.svg';
import { ReactComponent as MongoDBLogo } from '../assets/logos/mongodb.svg';
import { ReactComponent as PHPLogo } from '../assets/logos/php.svg';
import { ReactComponent as DatabaseLogo } from '../assets/logos/database.svg';
import { ReactComponent as PythonLogo } from '../assets/logos/python.svg';
import { ReactComponent as LinuxLogo } from '../assets/logos/linux.svg';
import { ReactComponent as GitLogo } from '../assets/logos/git.svg';
import { ReactComponent as LaravelLogo } from '../assets/logos/laravel.svg';

export interface Skill {
  name: string,
  logo: any
}

export const Skills: Skill[] = [
  {
    name: "HTML 5",
    logo: HTML5Logo
  },
  {
    name: "CSS 3",
    logo: CSS3Logo
  },
  {
    name: "JavaScript",
    logo: JavaScriptLogo
  },
  {
    name: "React",
    logo: ReactLogo
  },
  {
    name: "React Redux",
    logo: ReduxLogo
  },
  {
    name: "Node.js",
    logo: NodeJSLogo
  },
  {
    name: "PHP",
    logo: PHPLogo
  },
  {
    name: "Oracle SQL",
    logo: DatabaseLogo
  },
  {
    name: "Python 3",
    logo: PythonLogo
  },
  {
  name: "GNU/Linux",
    logo: LinuxLogo
  },
  {
  name: "Git",
    logo: GitLogo
  },
  {
  name: "Laravel",
    logo: LaravelLogo
  }
];

export default Skills;
