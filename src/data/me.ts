import MyPicture from '../assets/me.jpg';
import { ReactComponent as GithubLogo } from '../assets/logos/github.svg';
import { ReactComponent as EmailLogo } from '../assets/logos/email.svg';
import { ReactComponent as FacebookLogo } from '../assets/logos/facebook.svg';
import { ReactComponent as WhatsappLogo } from '../assets/logos/whatsapp.svg';
import { ReactComponent as LinkedInLogo } from '../assets/logos/linkedin.svg';

export interface Link {
  name: string,
  href: string,
  logo: any
}

export interface DeveloperInfo {
  name: string;
  email: string;
  phone_number: string;
  picture: string;
  description: string[];
  links: Link[];
}

export const MyInfo: DeveloperInfo = {
  name: "Eduardo Hoefel",
  email: "me.ehoefel@gmail.com",
  phone_number: "+31 6 12 62 24 42",
  picture: MyPicture,
  description: [
    "Cyber Security student at Radboud University and living in Nijmegen, Netherlands.",
    "Currently writing my thesis on the Mental Poker problem: how to ensure a safe online card game without the use of trusted third-parties? This problem is solved with cryptographic protocols based on probabilistic encryption. I expect to finish the thesis in September 2021.",
    "I have three years of experience as a full-stack software engineer. You can find more information on my skills below.",
    "On the free time you can find me working with opensource projects, creating a tool to ease/automatize a task, or playing with Code Golf languages.",
    ],
  links: [
    {
      name: "Github",
      href: "https://github.com/eduardohoefel",
      logo: GithubLogo
    },
    {
      name: "Email",
      href: "mailto:me.ehoefel@gmail.com",
      logo: EmailLogo
    },
    {
      name: "LinkedIn",
      href: "https://www.linkedin.com/in/eduardo-hoefel-53a1a3a4/",
      logo: LinkedInLogo
    }
  ]
};

export default MyInfo;
