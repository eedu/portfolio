import IvrEditorScreenshot from '../assets/projects screenshots/ivr-editor.png';
import UnFTScreenshot from '../assets/projects screenshots/unft.png';
import JAELScreenshot from '../assets/projects screenshots/jael.png';
import JARDEScreenshot from '../assets/projects screenshots/jarde.png';
import DEARScreenshot from '../assets/projects screenshots/dear.png';

export interface ProjectCardProps {
  name: string;
  description: string;
  screenshot: string;
  darkScreenshot?: string;
  frontend?: string[];
  backend?: string[];
  db?: string[];
  href?: string;
  github?: string;
}

const projects: ProjectCardProps[] = [
  {
    name: 'DEAR-tool',
    description: "CLI for running experiments with probability density estimation",
    screenshot: DEARScreenshot,
    backend: ['Python'],
    github: 'https://github.com/eduardohoefel/DEAR-tool'
  },
  {
    name: 'JARDE',
    description: "Just another remote desktop environment. \n Unfinished project",
    screenshot: JARDEScreenshot,
    frontend: ['React', 'Redux'],
    backend: ['Nodejs', 'Bash'],
    github: 'https://github.com/eduardohoefel/JARDE'
  },
  {
    name: 'IVR Editor',
    description: 'IVR drag-and-drop editor app that should be easy to integrate with Asterisk',
    screenshot: IvrEditorScreenshot,
    frontend: ['React', 'Redux']
  },
  {
    name: 'JAEL',
    description: 'Esoteric programming language for Code Golf',
    screenshot: JAELScreenshot,
    backend: ['Python'],
    github: 'https://github.com/eduardohoefel/JAEL'
  },
  {
    name: 'UnFT',
    description: 'Project that aims to build forms available for multiple platforms',
    screenshot: UnFTScreenshot,
    backend: ['Python', 'Ncurses'],
    github: 'https://github.com/eduardohoefel/UnFT'
  }
];

export default projects;
